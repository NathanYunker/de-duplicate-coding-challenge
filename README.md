# README #

### De-Duplicate Coding Challenge ###

* A small application to remove duplicate records in a JSON file.

### Set up ###

* With Java and Maven installed and configured to your system:

* Run `mvn package` to compile the project and run tests
* Run `mvn clean test` to run tests
* Run the command `java -cp lib/jackson-core-2.2.3.jar;lib/jackson-databind-2.2.3.jar;lib/jackson-annotations-2.2.3.jar;target/de-duplicate-1.0-SNAPSHOT.jar com.domain.app.DeDuplicateJSON` to run the application

* To change the data set being worked on, change the file contents in the JSONSource directory, located in the root directory.
* After running the application, all input, output and line item changes are logged to LeadChanges.txt file in the logs directory, located in the root directory.