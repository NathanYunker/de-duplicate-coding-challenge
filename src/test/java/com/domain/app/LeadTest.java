package com.domain.app;

import org.junit.Test;
import static org.junit.Assert.*;


public class LeadTest {

    @Test
    public void shouldProperlyCreateLeadObject() {
    	Lead lead = new Lead();
    	lead.set_id("jkjk12345jkjk12345");
    	lead.setEmail("george@washington.gov");
    	lead.setFirstName("George");
    	lead.setLastName("Washington");
    	lead.setAddress("Mt. Vernon");
    	lead.setEntryDate("1732-02-22T00:00-04:00");
    	
    	
    	assertTrue(lead.get_id().equals("jkjk12345jkjk12345"));
    	assertTrue(lead.getEmail().equals("george@washington.gov"));
        assertTrue( lead.getFirstName().equals("George"));
        assertTrue(lead.getLastName().equals("Washington"));
        assertTrue(lead.getAddress().equals("Mt. Vernon"));
        assertTrue(lead.getEntryDate().equals("1732-02-22T00:00-04:00"));
    }
}
