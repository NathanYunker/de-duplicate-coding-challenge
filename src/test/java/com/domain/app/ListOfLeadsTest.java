package com.domain.app;

import org.junit.Test;
import static org.junit.Assert.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.ArrayList;


public class ListOfLeadsTest {

    @Test
    public void shouldProperlyCreateListOfLeadsObject() {
    	ListOfLeads testList = new ListOfLeads();
    	
    	Lead lead1 = new Lead();
    	lead1.set_id("kjkj54321kjkj54321");
    	lead1.setEmail("john@adams.gov");
    	lead1.setFirstName("John");
    	lead1.setLastName("Adams");
    	lead1.setAddress("Peacefield");
    	lead1.setEntryDate("1735-10-30T00:00-04:00");
    	
    	Lead lead2 = new Lead();
    	lead2.set_id("12345jkjk12345jkjk");
    	lead2.setEmail("thomas@jefferson.gov");
    	lead2.setFirstName("Thomas");
    	lead2.setLastName("Jefferson");
    	lead2.setAddress("Monticello");
    	lead2.setEntryDate("1743-04-13T00:00-04:00");
    	
    	ArrayList<Lead> listToAdd = new ArrayList<Lead>();
    	listToAdd.add(lead1);
    	listToAdd.add(lead2);
    	testList.setLeads(listToAdd);
    	
    	
    	assertThat(testList.getLeads().size(), is(2));
    	assertThat(testList.getLeads(), contains(lead1, lead2));
    	assertTrue(testList.getLeads().get(0).getFirstName().equals("John"));
    	assertTrue(testList.getLeads().get(1).getFirstName().equals("Thomas"));
    }
}
