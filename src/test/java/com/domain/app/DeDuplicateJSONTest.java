package com.domain.app;

import org.junit.Test;
import static org.junit.Assert.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;


public class DeDuplicateJSONTest {
	
	ArrayList<Lead> listToAdd = new ArrayList<Lead>();
	Lead lead1 = new Lead();
	Lead lead2 = new Lead();
	ListOfLeads testList = new ListOfLeads();
	Set<Lead> deDuplicatedList = new LinkedHashSet<Lead>();
	
	private void initializeTestData() {
		
    	lead1.set_id("98789fd45we46we4wa5");
    	lead1.setEmail("foo@bar.com");
    	lead1.setFirstName("John");
    	lead1.setLastName("Smith");
    	lead1.setAddress("123 Street St");
    	lead1.setEntryDate("2014-05-07T17:30:20+00:00");
    	
    	lead2.set_id("jkj238238jdsnfsj23");
    	lead2.setEmail("coo@bar.com");
    	lead2.setFirstName("Ted");
    	lead2.setLastName("Jones");
    	lead2.setAddress("456 Neat St");
    	lead2.setEntryDate("2014-05-07T17:32:20+00:00");
	}
	
	private void setDeDuplicatedList() {
		
		listToAdd.clear();
		listToAdd.add(lead1);
		listToAdd.add(lead2);
		testList.setLeads(listToAdd);
		deDuplicatedList = DeDuplicateJSON.logNewestLeads(testList);

	}

    @Test
    public void shouldRemoveLeadWithDuplicateIdsKeepingNewestRecord() {
    	
    	initializeTestData();
    	lead1.set_id("jkj238238jdsnfsj23");
    	setDeDuplicatedList();
    	
    	assertThat(deDuplicatedList.size(), is(1));
    	assertThat(deDuplicatedList, contains(lead2));
    	
    	lead1.setEntryDate("2014-05-07T17:40:20+00:00");
    	setDeDuplicatedList();
    	
    	assertThat(deDuplicatedList.size(), is(1));
    	assertThat(deDuplicatedList, contains(lead1));
    }
    
    @Test
    public void shouldRemoveLeadWithDuplicateEmailssKeepingNewestRecord() {
    	
    	initializeTestData();
    	lead1.setEmail("coo@bar.com");
    	setDeDuplicatedList();
    	
    	assertThat(deDuplicatedList.size(), is(1));
    	assertThat(deDuplicatedList, contains(lead2));
    	
    	lead1.setEntryDate("2014-05-07T17:45:20+00:00");
    	setDeDuplicatedList();
    	
    	assertThat(deDuplicatedList.size(), is(1));
    	assertThat(deDuplicatedList, contains(lead1));
    }
    
    @Test
    public void shouldNotRemoveLeadWithDuplicateFirstNames() {
    	
    	initializeTestData();
    	lead1.setFirstName("Ted");
    	setDeDuplicatedList();
    	
    	assertThat(deDuplicatedList.size(), is(2));
    	assertThat(deDuplicatedList, contains(lead1, lead2));
    }
    
    @Test
    public void shouldNotRemoveLeadWithDuplicateLastNames() {
    	
    	initializeTestData();
    	lead1.setFirstName("Jones");
    	setDeDuplicatedList();
    	
    	assertThat(deDuplicatedList.size(), is(2));
    	assertThat(deDuplicatedList, contains(lead1, lead2));
    }
    
    @Test
    public void shouldNotRemoveLeadWithDuplicateAddresses() {
    	
    	initializeTestData();
    	lead1.setFirstName("456 Neat St");
    	setDeDuplicatedList();
    	
    	assertThat(deDuplicatedList.size(), is(2));
    	assertThat(deDuplicatedList, contains(lead1, lead2));
    }
    
    @Test
    public void shouldPreferLastEntryInDataSetIfDatesAreTheSame() {
    	
    	initializeTestData();
    	lead1.setEmail("coo@bar.com");
    	lead1.setEntryDate("2014-05-07T17:32:20+00:00");
    	setDeDuplicatedList();
    	
    	assertThat(deDuplicatedList.size(), is(1));
    	assertThat(deDuplicatedList, contains(lead2));
    	
    	initializeTestData();
    	lead1.set_id("jkj238238jdsnfsj23");
    	lead1.setEntryDate("2014-05-07T17:32:20+00:00");
    	setDeDuplicatedList();
    	
    	assertThat(deDuplicatedList.size(), is(1));
    	assertThat(deDuplicatedList, contains(lead2));
    }
}
