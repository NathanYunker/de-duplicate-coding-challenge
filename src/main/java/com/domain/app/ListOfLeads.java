package com.domain.app;

import java.util.List;

//Wrapper class to aid in JSON parsing
public class ListOfLeads {

	private List<Lead> leads;

	public void setLeads(List<Lead> leads) {
		this.leads = leads;
	}

	public List<Lead> getLeads() {
		return this.leads;
	}
}
