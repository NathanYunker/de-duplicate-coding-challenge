package com.domain.app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import com.fasterxml.jackson.databind.ObjectMapper;

public class DeDuplicateJSON {

	private static final Logger LOGGER = Logger.getLogger(DeDuplicateJSON.class.getName());
	// A multidimensional array, with the inner array containing the older
	// record a 0 index and its updated record at index 1
	static private ArrayList<ArrayList<Lead>> duplicateRecordsMatrix = new ArrayList<ArrayList<Lead>>();

	public static void main(String[] args) throws IOException {

		setLogging();

		// read json file data to String
		byte[] jsonData = Files.readAllBytes(Paths.get(System.getProperty("user.dir") + "/JSONSource/leads.json"));

		// create ObjectMapper instance
		ObjectMapper objectMapper = new ObjectMapper();

		// convert json string to object
		ListOfLeads listOfLeads = objectMapper.readValue(jsonData, ListOfLeads.class);

		logNewestLeads(listOfLeads);

	}
	
	/**
	 *
	 * Prints source records, finds newest unique records and prints them, and
	 * then prints line by line record changes
	 *
	 * @param listOfLeads - the list of Leads being to check for duplicates
	 * @return - a set of Leads with duplicates removed, keeping the newest entry dates
	 */
	public static Set<Lead> logNewestLeads(ListOfLeads listOfLeads) {

		LOGGER.info("------------------------------Source Records--------------------------------");
		for (Lead lead : listOfLeads.getLeads()) {
			LOGGER.info(lead.toString());
		}

		Set<Lead> newestUniqueRecords = updateListToMostRecentRecords(listOfLeads.getLeads());

		LOGGER.info("----------------------------Output Records-----------------------------------");
		for (Lead uniqLead : newestUniqueRecords) {
			LOGGER.info("unique Lead: " + uniqLead.toString());
		}

		printOutDiff();
		return newestUniqueRecords;
	}
	
	/**
	 *
	 * Take a list of Lead objects, removing all objects with duplicate ids or emails.
	 *
	 * @param listOfLeadsToCheck - the list being checked for duplicates
	 * @return - a set of leads with no duplicated ids or emails
	 */
	private static Set<Lead> updateListToMostRecentRecords(List<Lead> listOfLeadsToCheck) {
		Set<Lead> uniqueRecords = new LinkedHashSet<Lead>();
		uniqueRecords = removeRecordsWithDuplicateIds(listOfLeadsToCheck);
		listOfLeadsToCheck.clear();
		listOfLeadsToCheck.addAll(uniqueRecords);
		uniqueRecords = removeRecordsWithDuplicateEmails(listOfLeadsToCheck);

		return uniqueRecords;
	}

	/**
	 *
	 * Iterates through a list of lead objects, removing those with duplicate emails.
	 *
	 * @param listOfLeadsToCheck - the list being checked for duplicates
	 * @return - a set of leads with no duplicated emails
	 */
	private static Set<Lead> removeRecordsWithDuplicateEmails(List<Lead> listOfLeads) {

		Set<Lead> uniqueRecords = new LinkedHashSet<Lead>();
		Set<String> uniqueEmails = new HashSet<String>();
		Lead leadInSet = new Lead();

		for (int i = 0; i < listOfLeads.size(); i++) {
			if (!uniqueEmails.add(listOfLeads.get(i).getEmail())) {
				for (Lead uniqLead : uniqueRecords) {
					if (uniqLead.getEmail().equals(listOfLeads.get(i).getEmail())) {
						leadInSet = uniqLead;
					}
				}

				uniqueRecords = compareEntryDatesAndReturnNewest(uniqueRecords, listOfLeads.get(i), leadInSet);
			} else {
				uniqueRecords.add(listOfLeads.get(i));
			}
		}
		return uniqueRecords;
	}

	/**
	 *
	 * Iterates through a list of lead objects, removing those with duplicate
	 * Ids.
	 *
	 * @param listOfLeadsToCheck
	 *            - the list being checked for duplicates
	 * @return - a set of leads with no duplicated ids
	 */
	private static Set<Lead> removeRecordsWithDuplicateIds(List<Lead> listOfLeads) {

		Set<Lead> uniqueRecords = new LinkedHashSet<Lead>();
		Set<String> uniqueIds = new HashSet<String>();
		Lead leadInSet = new Lead();

		for (int i = 0; i < listOfLeads.size(); i++) {
			if (!uniqueIds.add(listOfLeads.get(i).get_id())) {
				for (Lead uniqLead : uniqueRecords) {
					if (uniqLead.get_id().equals(listOfLeads.get(i).get_id())) {
						leadInSet = uniqLead;
					}
				}

				uniqueRecords = compareEntryDatesAndReturnNewest(uniqueRecords, listOfLeads.get(i), leadInSet);
			} else {
				uniqueRecords.add(listOfLeads.get(i));
			}
		}

		return uniqueRecords;

	}
	
	/**
	 *
	 * Compares two Lead objects and returns the one with the most recent entry
	 * date. If the entry dates are the same, will return the current lead.
	 *
	 * @param uniqueRecords - the list of Leads being altered
	 * @param currentLead - The current lead being used to compare, it will be the later entry in the set
	 * @param leadInSet - The Lead in the "unique" set which the current lead is being compared against
	 * @return - a set of Leads with the newest entry dates
	 */
	private static Set<Lead> compareEntryDatesAndReturnNewest(Set<Lead> uniqueRecords, Lead currentLead,
			Lead leadInSet) {
		Date dupLeadEntryDate;
		Date uniqLeadEntryDate;
		ISO8601DateFormat dateTimeFormat = new ISO8601DateFormat();
		try {
			dupLeadEntryDate = dateTimeFormat.parse(currentLead.getEntryDate());
			uniqLeadEntryDate = dateTimeFormat.parse(leadInSet.getEntryDate());
			if (dupLeadEntryDate.after(uniqLeadEntryDate) || dupLeadEntryDate.compareTo(uniqLeadEntryDate) == 0) {
				uniqueRecords.remove(leadInSet);
				uniqueRecords.add(currentLead);

				ArrayList<Lead> diffArray = new ArrayList<Lead>();
				diffArray.add(leadInSet);
				diffArray.add(currentLead);
				duplicateRecordsMatrix.add(diffArray);
			}
		} catch (ParseException e) {
			// LOGGER.warning("Unable To Parse Date");
			e.printStackTrace();
		}

		return uniqueRecords;

	}

	/**
	 *
	 * Constructs a string and logs it out, showing a line by line change of
	 * object properties with the same id or email from one entry date to the
	 * next. If property did not change will print only the original property
	 * value.
	 *
	 */
	private static void printOutDiff() {

		for (ArrayList<Lead> arrayOfDiffs : duplicateRecordsMatrix) {

			StringBuilder sb = new StringBuilder();

			if (!arrayOfDiffs.get(0).get_id().equals(arrayOfDiffs.get(1).get_id())) {
				sb.append("-ID=" + arrayOfDiffs.get(0).get_id() + "----changed to----" + arrayOfDiffs.get(1).get_id()
						+ ". \n");
			} else {
				sb.append("-ID=" + arrayOfDiffs.get(0).get_id() + " \n");
			}

			if (!arrayOfDiffs.get(0).getEmail().equals(arrayOfDiffs.get(1).getEmail())) {
				sb.append("-Email=" + arrayOfDiffs.get(0).getEmail() + "----changed to----"
						+ arrayOfDiffs.get(1).getEmail() + ". \n");
			} else {
				sb.append("-Email=" + arrayOfDiffs.get(0).getEmail() + " \n");
			}

			if (!arrayOfDiffs.get(0).getFirstName().equals(arrayOfDiffs.get(1).getFirstName())) {
				sb.append("-FirstName=" + arrayOfDiffs.get(0).getFirstName() + "----changed to----"
						+ arrayOfDiffs.get(1).getFirstName() + "\n");
			} else {
				sb.append("-FirstName=" + arrayOfDiffs.get(0).getFirstName() + " \n");
			}

			if (!arrayOfDiffs.get(0).getLastName().equals(arrayOfDiffs.get(1).getLastName())) {
				sb.append("-LastName=" + arrayOfDiffs.get(0).getLastName() + "----changed to----"
						+ arrayOfDiffs.get(1).getLastName() + "\n");
			} else {
				sb.append("-LastName=" + arrayOfDiffs.get(0).getLastName() + " \n");
			}

			if (!arrayOfDiffs.get(0).getAddress().equals(arrayOfDiffs.get(1).getAddress())) {
				sb.append("-Address=" + arrayOfDiffs.get(0).getAddress() + "----changed to----"
						+ arrayOfDiffs.get(1).getAddress() + ".\n");
			} else {
				sb.append("-Address=" + arrayOfDiffs.get(0).getAddress() + " \n");
			}

			sb.append("-Original Entry Date=" + arrayOfDiffs.get(0).getEntryDate() + "----Updated EntryDate="
					+ arrayOfDiffs.get(1).getEntryDate() + "\n");

			LOGGER.info(
					"------------------------------Record Updated--------------------------------\n" + sb.toString());
		}

	}

	/**
	 *
	 * Sets up the Logger for the application, specifing all loging levels should be shown, 
	 * and adding a fileHandler to write logs to the logs director, and a StreamHandler to 
	 * print as System.out rather than System.err
	 *
	 */
	private static void setLogging() {
		LOGGER.setLevel(Level.ALL);
		FileHandler fh = null;

		try {
			fh = new FileHandler(System.getProperty("user.dir") + "/logs/LeadChanges.log");
		} catch (Exception e) {
			e.printStackTrace();
		}

		fh.setFormatter(new SimpleFormatter());
		LOGGER.addHandler(fh);
		SimpleFormatter fmt = new SimpleFormatter();
		StreamHandler sh = new StreamHandler(System.out, fmt);
		LOGGER.addHandler(sh);
		LOGGER.setUseParentHandlers(false);

	}
}
