package com.domain.app;

//POJO Lead Class
public class Lead {

	private String _id;
	private String email;
	private String firstName;
	private String lastName;
	private String address;
	private String entryDate;

	public String get_id() {
		return _id;
	}

	public void set_id(String id) {
		this._id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("***** Lead Details *****\n");
		sb.append("-ID=" + get_id() + "\n");
		sb.append("-Email=" + getEmail() + "\n");
		sb.append("-FirstName=" + getFirstName() + "\n");
		sb.append("-LastName=" + getLastName() + "\n");
		sb.append("-Address=" + getAddress() + "\n");
		sb.append("-Entry Date=" + getEntryDate() + "\n");
		sb.append("*****************************");

		return sb.toString();
	}
}
